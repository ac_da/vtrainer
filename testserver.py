#!/usr/bin/env python2

import hashlib
import os
import os.path
import random
import sys
import SimpleHTTPServer
import BaseHTTPServer
import SocketServer

PORT = 7890

PREREAD_FILES = False

VOCABULARY_FILE = "./Fr.txt"

AUTH_PASSWORD = "blubb"

class Ventry(object):
	__slots__ = ('ID','word','expl','sample','pos','history','passok')

	def __init__(self):
		self.ID = None
		self.word = None
		self.expl = None
		self.sample = None
		self.pos = None
		self.history = None
		self.passok = False

# Table columns:  ID | word | expl | sample | pos | history

VTable = dict()

RANDblocks = list()

default_page_html = """<!DOCTYPE html>
<html>
<meta charset="ISO-8859-1"/>
<head>
<style type=text/css>
div.ex1
{
  background-color: lightblue;
  width: 100%;
  height: 240px;
  overflow: scroll;
}
body {color: #CCCCCC}
</style>
</head>
<body bgcolor="#000000">
<p>start here:</br>
<a href="vtrain.html">./train.html</a></br>
</p>
</body>
</html>
"""

def read_table_file(filename):
	f = file(filename,"rb")
	res = dict()
	noID = list()
	lineNo=0
	for line in f:
		line = unicode(line.strip(),"utf-8","ignore")
		lineNo = lineNo+1
		if line=="" or line.startswith(u"#"):
			continue
		v = parse_input_line(line)

		if v.word == "" or v.expl == "":
			sys.stderr.write("tablefile error, entry without a word line %d."%(lineNo,))
			continue

		if v.ID is not None:
			if v.ID in res:
				sys.stderr.write("parsing tablefile error, duplicate ID(%d)  in line #%d\n"%(v.ID,lineNo))
				continue
			res[v.ID] = v
		else:
			noID.append(v)
	f.close()
	if len(noID)>0:
		sys.stderr.write("parsing tablefile: some items have no ID\n")
	nextID=1
	noID.sort()
	while len(noID)>0:
		v = noID[-1]
		del noID[-1]
		while nextID in res:
			nextID = nextID+1
		v.ID = nextID
		res[v.ID] = v
	print("read file done, have %d items."%(len(res),))
	return res

def parse_input_line(line):
	line = line.strip().split(u"|")[:6]
	while len(line)<6:
		line.append(u"")
	v = Ventry()
	chk=0
	v.word = line[1].strip()
	v.expl = line[2].strip()
	v.sample = line[3].strip()
	v.ID = None
	if len(line[0])>0:
		chk-=1
		try:
			v.ID=int(line[0],0)
		except ValueError:
			pass
		chk+=1
	if len(line[4])>0:
		chk-=1
		try:
			v.pos=int(line[4],0)
		except ValueError:
			pass
		chk+=1
	v.history = line[5].strip()
	if chk!=0:
		return None
	return v

def write_table_file(filename,allitemsdict):
	lines = list()
	IDs = allitemsdict.keys()
	IDs.sort()
	for ID in IDs:
		v = allitemsdict[ID]
		_tmp = list()
		_pos = ''
		if v.pos is not None:
			_pos = str(v.pos)
		_tmp.append(strip_entry("%3d"%v.ID,5))
		_tmp.append(strip_entry(v.word,35))
		_tmp.append(strip_entry(v.expl,55))
		_tmp.append(strip_entry(v.sample,5))
		_tmp.append(strip_entry(_pos,4))
		_tmp.append(strip_entry(v.history[:16]))
		lines.append("|".join(_tmp))
	f = file(filename,"w")
	for _tmp in lines:
		f.write(_tmp.encode('utf-8')+"\n")
	f.close()


def strip_entry(entr,minlen=0):
	entr = unicode(entr)
	entr.replace(u"|",u"_")
	entr.replace(u"\r",u"")
	entr.replace(u"\n",u" ")
	entr.replace(u"\t",u" ")
	entr.replace(u"\x00",u"")
	if len(entr)<minlen:
		entr = entr + (u" "*(minlen-len(entr)))
	return entr

# taken from  https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types
MIMEtab = {
	"gif":"image/gif",
	"htm":"text/html",
	"html":"text/html",
	"jpeg":"image/jpeg",
	"jpg":"image/jpeg",
	"js":"text/javascript",
	"mp3":"audio/mpeg",
	"pdf":"application/pdf",
	"png":"image/png",
	"txt":"text/plain",
}

class VHandler(BaseHTTPServer.BaseHTTPRequestHandler):
	__slots__ = ("reqbase","reqargs","postdata")
	def __init__(self,request,client_address,server):
		BaseHTTPServer.BaseHTTPRequestHandler.__init__(self,request,client_address,server)

	def do_GET(self):
		# Here, the self.path is the request, such as:   "GET /blubb.html?a=x HTTP/1.1"
		print("got GET request: "+self.path)

		self.reqbase,self.reqargs = split_args(self.path)
		self.passok = False
		print("req=%s  args=%s"%(repr(self.reqbase),repr(self.reqargs)))
		if self.reqbase.lower().startswith("/php/getvlist.php"):
			return self.do_GET_getvlist()
		if self.reqbase in staticFilesDict:
			return self.do_GET_html()
		if self.reqbase.lower().startswith("/php/getchall.php"):
			return self.do_GET_getchall()

		if self.reqbase.lower().startswith("/php/newword.php"):
			print("newword")


		self.send_response(200)
		self.send_header("Content-type","text/html")
		self.end_headers()
		self.wfile.write(default_page_html)

		return

	def do_POST(self):
		print("got POST request: "+self.path)

		self.reqbase,self.reqargs = split_args(self.path)
		self.passok = False
		print("req=%s  args=%s"%(repr(self.reqbase),repr(self.reqargs)))

		try:
			content_len = int(self.headers.getheader('content-length', 0))
		except ValueError:
			self.send_response(400)
			self.end_headers()
			self.wfile.write("Cannot parse 'content-length'.")
			return
		post_body = self.rfile.read(content_len)

		# check password
		if check_args_authkey(self.reqargs):
			print("password is correct.")
			self.passok = True
		else:
			print("invalid password.")

		try:
			post_body = post_body.decode("utf-8")
		except UnicodeDecodeError:
			print("error UTF-8 decoding data from POST.")
			# ..... errormessage in html reply.
			self.send_response(400)
			self.end_headers()
			self.wfile.write("Cannot decode content as utf8.")
			return

		print("read POST bodydata, %d bytes. %s"%(content_len,repr(post_body[:30])))
		self.postdata = post_body

		if self.reqbase.lower().startswith("/php/newstate.php"):
			return self.do_POST_newstate()

		if self.reqbase.lower().startswith("/php/newword.php"):
			return self.do_POST_newword()

		self.send_response(404)
		self.end_headers()
		self.wfile.write("unknown post request for "+self.reqbase)
		self.postdata = None
		return


	def do_GET_getvlist(self):
		print("getvlist")
		maxpos = None
		if "maxpos" in self.reqargs:
			try:
				maxpos = int(self.reqargs["maxpos"],10)
			except ValueError:
				pass
		self.send_response(200)
		self.send_header("Content-type","text/plain")
		self.end_headers()
		self.wfile.write(u"\uFEFF".encode("utf-8"))  # send UTF-8 BOM

		for ID in VTable:
			v = VTable[ID]
			if (maxpos is None) or (v.pos<=maxpos):
				line = u"%d|%s|%s|%s|%d|%s\n"%(v.ID,v.word,v.expl,v.sample,v.pos,v.history)
				self.wfile.write(line.encode("utf-8"))
		self.wfile.write("\n")

	def do_GET_getchall(self):
		# Create a new challenge (never return the same twice)
		ch = list()
		for i in range(16):
			ch.append("%04X"%random.randint(0,0xFFFF))
		ch = "".join(ch)
		auth = hashlib.sha256(ch+AUTH_PASSWORD).hexdigest().lower().strip()
		RANDblocks.append((ch,(auth,)))
		# Drop old if too many
		if len(RANDblocks)>128:
			del RANDblocks[0]
		# ack and send it.
		self.send_response(200)
		self.send_header("Content-type","text/plain")
		self.end_headers()
		self.wfile.write(RANDblocks[-1][0].encode("utf-8"))  # send UTF-8 BOM
		return

	def do_GET_html(self):
		fpath,data = staticFilesDict[self.reqbase]
		if data is None:
			try:
				f = file(fpath,"rb")
				data = f.read(0x400000)
				f.close()
			except IOError:
				self.send_response(404)
				self.end_headers()
				return
		ending = self.reqbase.split(".")[-1].lower()
		if ending not in MIMEtab:
			ending = "txt"
		self.send_response(200)
		self.send_header("Content-type",MIMEtab[ending])
		self.end_headers()
		if ending == "txt":
			self.wfile.write(u"\uFEFF".encode("utf-8"))  # send UTF-8 BOM
		self.wfile.write(data)

	def do_POST_newstate(self):
		print("processing POST newstate")

		if not self.passok:
			self.send_response(401)
			self.send_header("Content-type","text/plain")
			self.end_headers()
			self.wfile.write("no valid password supplied.")
			return

		countUpd = 0
		for line in self.postdata.split(u"\n"):
			line = line.strip()
			if line=="" or line.startswith(u"#"):
				continue
			vn = parse_input_line(line)
			if vn is None:
				print("WARN: cannot parse input line from client: "+repr(line))
				continue	# parse error.
			if vn.ID not in VTable:
				print("WARN: client sends update for non-existend item %d"%(vn.ID,))
				continue
			v = VTable[vn.ID]
			_chg = False
			if vn.pos!=v.pos:
				v.pos = vn.pos
				_chg=True
			if vn.history is not None and vn.history!="" and vn.history!=v.history:
				v.history = vn.history
				_chg=True
			if vn.sample is not None and vn.sample!="" and vn.sample!=v.history:
				v.sample = vn.sample
				_chg=True
			if _chg:
				countUpd += 1
				#print("item #%d updated."%(vn.ID,))

		self.postdata = None

		res = "%d items updates."%(countUpd,)
		print(res)

		self.send_response(200)
		self.send_header("Content-type","text/plain")
		self.end_headers()
		self.wfile.write(res)

		write_table_file(VOCABULARY_FILE,VTable)

		return


	def do_POST_newword(self):
		print("processing POST newword")

		if not self.passok:
			self.send_response(401)
			self.send_header("Content-type","text/plain")
			self.end_headers()
			self.wfile.write("no valid password supplied.")
			return

		freeID = 1
		_numadd = 0
		for line in self.postdata.split(u"\n"):
			line = line.strip()
			if line=="" or line.startswith(u"#"):
				continue
			vn = parse_input_line(line)
			if vn is None:
				print("WARN: cannot parse input line from client: "+repr(line))
				continue	# parse error.

			if vn.word=="" or vn.expl=="":
				print("WARN: new word sent, but field is empty.")
				continue	# parse error.
				#print("item #%d updated."%(vn.ID,))
			while freeID in VTable:
				freeID += 1
			vn.pos = 0
			vn.ID = freeID
			VTable[freeID] = vn
			_numadd += 1

		self.postdata = None

		res = "%d items added to table."%(_numadd,)
		print(res)

		self.send_response(200)
		self.send_header("Content-type","text/plain")
		self.end_headers()
		self.wfile.write(res)

		write_table_file(VOCABULARY_FILE,VTable)

		return



def split_args(line):
	_q = line.find("?")
	if _q<0:
		return line,dict()
	_pp = line[_q+1:].split("&")
	_args = dict()
	for _arg in _pp:
		_tmp = _arg.split("=")
		if len(_tmp)!=2:
			continue
		_argnam,_arg = tuple(_tmp)
		if _argnam not in _args:
			_args[_argnam] = _arg

	return line[:_q],_args

def check_args_authkey(args):
	""" Checks if the args contain a valid authentication. """
	if not "auth" in args:
		return False
	auth = args["auth"].lower().strip()
	# The value of the 'auth' arg must simply match the hexdigest of one of the challenges.
#	print("checking.\n%s\n%s\n"%(repr(RANDblocks),repr(args)))
	for i in xrange(len(RANDblocks)):
		ch,chauth = RANDblocks[i]
		for _auth in chauth:
			if _auth==auth:
				del RANDblocks[i]  # allow that only once.
				return True
	return False

def read_dirs_callback(reslist,dirname,names):
	for fil in names:
		fpath = os.path.join(dirname,fil)
		if os.path.isfile(fpath):
			print(fpath)
			data = None
			if PREREAD_FILES:
				f = file(fpath,"rb")
				data = f.read(0x400000)
				f.close()
			reslist.append((fpath,data))


def read_html_folder():
	basepath = "./html/"
	_tmp = list()
	os.path.walk(basepath,read_dirs_callback,_tmp)
	res = dict()
	for fpath,data in _tmp:
		wpath = fpath
		if wpath.startswith("."):
			wpath = wpath[1:]
		if wpath.startswith("/html"):
			wpath = wpath[5:]
		res[wpath] = (fpath,data)
	return res

#Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
#Handler = CGIHTTPServer.CGIHTTPRequestHandler
#Handler = PHPHandler
Handler = VHandler

VTable = read_table_file(VOCABULARY_FILE)
#write_table_file(VOCABULARY_FILE,VTable)

staticFilesDict = read_html_folder()

SocketServer.TCPServer.allow_reuse_address = True
httpd = SocketServer.TCPServer(("0.0.0.0", PORT), Handler)

print("serving at port",PORT)
httpd.serve_forever()


